package io.gitlab.chkypros.securityamigos.student;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@RestController
@RequestMapping("/api/v1/students")
public class StudentController {

    private static final List<Student> STUDENTS = new ArrayList<>(Arrays.asList(
        new Student(1, "Kypros"),
        new Student(2, "Elena"),
        new Student(3, "Nicoletta")
    ));

    @GetMapping("/{studentId}")
    public Student getStudent(@PathVariable("studentId") Integer id) {
        return STUDENTS.stream()
            .filter(s -> id.equals(s.getId()))
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Student " + id + " doesn't exist"));
    }
}
