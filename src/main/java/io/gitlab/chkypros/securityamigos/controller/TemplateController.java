package io.gitlab.chkypros.securityamigos.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Controller
public class TemplateController {

    @GetMapping("login")
    public String loginPage() {
        return "login";
    }

    @GetMapping("courses")
    public String coursesPage() {
        return "courses";
    }
}
