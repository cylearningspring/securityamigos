package io.gitlab.chkypros.securityamigos.auth;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface ApplicationUsersDao {
    Optional<ApplicationUser> findUserByUsername(String username);
}
