package io.gitlab.chkypros.securityamigos.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import io.gitlab.chkypros.securityamigos.security.UserRole;

/**
 * "Customized" implementation of {@link ApplicationUsersDao} without Spring Data
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Repository("fake")
public class ApplicationUsersDaoImpl implements ApplicationUsersDao {
    private static final List<ApplicationUser> STUDENTS = new ArrayList<>();

    @Autowired
    public ApplicationUsersDaoImpl(PasswordEncoder passwordEncoder) {
        STUDENTS.addAll(Arrays.asList(
            new ApplicationUser(
                "kypros",
                passwordEncoder.encode("password"),
                UserRole.ADMIN.getGrantedAuthorities(),
                true,
                true,
                true,
                true
            ),
            new ApplicationUser(
                "elena",
                passwordEncoder.encode("password"),
                UserRole.ADMINTRAINEE.getGrantedAuthorities(),
                true,
                true,
                true,
                true
            ),
            new ApplicationUser(
                "nicoletta",
                passwordEncoder.encode("pass123"),
                UserRole.STUDENT.getGrantedAuthorities(),
                true,
                true,
                true,
                true
            )
        ));
    }

    @Override
    public Optional<ApplicationUser> findUserByUsername(String username) {
        return STUDENTS.stream()
            .filter(s -> s.getUsername().equals(username))
            .findFirst();
    }
}
