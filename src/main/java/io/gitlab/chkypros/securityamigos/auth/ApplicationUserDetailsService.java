package io.gitlab.chkypros.securityamigos.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Service
public class ApplicationUserDetailsService implements UserDetailsService {
    private final ApplicationUsersDao applicationUsersDao;

    @Autowired
    public ApplicationUserDetailsService(ApplicationUsersDao applicationUsersDao) {
        this.applicationUsersDao = applicationUsersDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return applicationUsersDao.findUserByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found"));
    }
}
