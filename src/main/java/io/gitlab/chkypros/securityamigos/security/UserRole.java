package io.gitlab.chkypros.securityamigos.security;

import com.google.common.collect.Sets;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static io.gitlab.chkypros.securityamigos.security.UserPermission.COURSE_READ;
import static io.gitlab.chkypros.securityamigos.security.UserPermission.COURSE_WRITE;
import static io.gitlab.chkypros.securityamigos.security.UserPermission.STUDENT_READ;
import static io.gitlab.chkypros.securityamigos.security.UserPermission.STUDENT_WRITE;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public enum UserRole {
    ADMIN(Sets.newHashSet(STUDENT_READ, STUDENT_WRITE, COURSE_READ, COURSE_WRITE)),
    ADMINTRAINEE(Sets.newHashSet(STUDENT_READ, COURSE_READ)),
    STUDENT;

    private final Set<UserPermission> permissions;

    UserRole() {
        this(Sets.newHashSet());
    }

    UserRole(Set<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<UserPermission> getPermissions() {
        return permissions;
    }

    public Set<GrantedAuthority> getGrantedAuthorities() {
        Set<GrantedAuthority> grantedAuthorities = permissions.stream()
            .map(UserPermission::getPermission)
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toSet());
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));

        return grantedAuthorities;
    }
}
